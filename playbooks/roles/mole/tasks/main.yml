---
- name: Load variables from {{ mole_config_dir }}/vars.yml
  include_vars: '{{ mole_config_dir }}/vars.yml'

- name: Ensure docker-py present
  pip:
    # https://github.com/ansible/ansible-modules-core/issues/1227
    name: 'docker-py==1.1.0'
    state: present
  environment:
    no_proxy: '*'

- name: Ensure mole_volume_dir is a directory
  file:
    state: directory
    path: '{{ mole_volume_dir }}'

- name: Synchronize /etc
  synchronize:
    dest: '{{ mole_volume_dir }}'
    src: roles/mole/files/etc
    perms: no

- name: Ensure /etc/* are present and directories
  file:
    state: directory
    path: '{{ mole_volume_dir }}/etc/{{ item }}'
  with_items:
    - shadowsocks
    - cow
    - privoxy

- name: Copy config template
  template:
    dest: '{{ mole_volume_dir }}/etc/{{ item }}'
    src: 'etc/{{ item }}'
  with_items:
    - shadowsocks/config.json
    - cow/rc
    - cow/blocked
    - cow/direct

- name: Copy privoxy user.action templates
  copy:
    dest: '{{ mole_volume_dir }}/etc/privoxy/'
    src: 'etc/privoxy/user.action.d'

- name: Check if {{ mole_config_dir }}/user.action.d exists
  local_action:
    module: stat
    path: '{{ mole_config_dir }}/user.action.d'
    follow: yes
  register: st

- name: Assemble etc/privoxy/user.action.d/50body
  assemble:
    src: '{{ mole_config_dir }}/user.action.d'
    remote_src: False
    dest: '{{ mole_volume_dir }}/etc/privoxy/user.action.d/50body'
  when: st.stat.isdir is defined and st.stat.isdir

- name: Assemble etc/privoxy/user.action
  assemble:
    src: '{{ mole_volume_dir }}/etc/privoxy/user.action.d'
    remote_src: True
    dest: '{{ mole_volume_dir }}/etc/privoxy/user.action'

- name: Setup shadowsocks container
  docker:
    name: shadowsocks
    image: dochang/shadowsocks-libev:latest
    command: ss-local
    restart_policy: always
    expose:
      - '1080'
    volumes:
      - '{{ mole_volume_dir }}/etc/shadowsocks:/etc/shadowsocks'
    state: '{{ mole_state }}'

- name: Setup cow container
  docker:
    name: cow
    image: dochang/cow:latest
    restart_policy: always
    links:
      - shadowsocks:shadowsocks
    expose:
      - '7777'
    volumes:
      - '{{ mole_volume_dir }}/etc/cow:/.cow'
    state: '{{ mole_state }}'

- name: Setup tor container
  docker:
    name: tor
    image: dochang/tor:latest
    restart_policy: always
    links:
      - shadowsocks:shadowsocks
    expose:
      - '9050'
    volumes:
      - '{{ mole_volume_dir }}/etc/tor:/etc/tor'
    state: '{{ mole_state }}'

- name: Setup privoxy container
  docker:
    name: privoxy
    image: dochang/privoxy:latest
    restart_policy: always
    links:
      - cow:cow
      - shadowsocks:shadowsocks
      - tor:tor
    volumes:
      - '{{ mole_volume_dir }}/etc/privoxy:/etc/privoxy'
    env:
      TZ: 'Asia/Shanghai'
    ports:
      - '127.0.0.1:8118:8118'
    state: '{{ mole_state }}'
